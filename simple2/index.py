class State:
    def __init__(self):
        print('Processing current state:', str(self))

    def on_event(self, event):
        pass

    def __str__(self):
        return self.__class__.__name__


class LockedState(State):
    def on_event(self, event):
        if event == 'pin_entered':
            return UnlockedState()
        return self


class UnlockedState(State):
    def on_event(self, event):
        if event == 'device_locked':
            return LockedState()
        return self


class SimpleDevice:
    def __init__(self):
        self.state = LockedState()

    def on_event(self, event):
        self.state = self.state.on_event(event)



device = SimpleDevice()
device.on_event('device_locked')
device.on_event('pin_entered')
device.on_event('device_locked')
